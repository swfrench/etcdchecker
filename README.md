# An online etcd consistency checker

An experimental etcd cross-cluster consistency checker, supporting the v2 and v3
APIs.

**Note**: This is a pre-release proof-of-concept, and should not be used for
anything at this time.

`etcdchecker` verifies the consistency of keys (existence, content) under a
given prefix between a set of monitored etcd clusters.

The general use case is for verification of replication and / or data migration
correctness.

Both the v2 and v3 APIs are supported, under the assumption that an *identical
key structure* is used across all monitored clusters.

## Convergence

A key is considered "converged" if it is consistent in both existence and
content across all monitored clusters.

For each unconverged key, both the time in an unconverged state and the current
reason for unconvergence are reported.

Note: this simple notion of convergence does not measure consistency "delay" but
rather the time since list consistent state.

What this means is that a "hot" key (one updated more frequently than the delay
with which clusters become consistent, say due to replication), can appear
unconverged even though it's simply lagging behind.

An approach that treats key updates as a pipeline, in turn tracking delay
explicitly, is far more complex to implement and decouple from the specific
replication mechanism.

## Reporting

The following gauge-type metrics are exported:

* `etcdchecker_num_unconverged_keys` - The number of currently unconverged keys.
* `etcdchecker_max_convergence_delay_seconds` - The maximum time any currently
  unconverged key has been in that state.

In addition, the following are logged during export:

* The number of unconverged keys.
* The specific keys that have been unconverged for more than a configurable
  delay (i.e., "long unconverged").

## Configuration

Annotated configuration example:

```yaml
# clusters is a map from convenient name of a monitored cluster to client
# configuration used to interact with it.
clusters:
  cluster-a:
    # api is the etcd API version to use when interacting with these endpoints
    # (v2, v3).
    api: v2
    # endpoints is a list of endpoint URLs / hostports.
    endpoints:
      - node-a-1:2379
      - node-a-2:2379
      - node-a-3:2379
    # username is the username to use when authenticating to the cluster
    # (optional).
    username: "..."
    # password is the password to use when authenticating to the cluster
    # (optional).
    password: "..."
    # Optional path to root CA certificates to use when verifying certificates
    # presented by etcd nodes in this cluster.
    ca_cert: "..."
  cluster-b:
    api: v3
    endpoints:
      - node-b-1:2379
      - node-b-2:2379
      - node-b-3:2379
    username: "..."
    password: "..."
    ca_cert: "..."
# prefix is the monitored keyspace prefix (default: /).
prefix: /
# ignored_pattern is a regexp (RE2) used to ignore certain parts of the keyspace
# (optional).
ignored_pattern: "^/ignored$"
# report_interval is the duration between updates to the consistency prometheus
# metrics (and logging for long-unconverged keys) (default: 30s).
report_interval: 10s
# listen_address is the address on which the checker spawns an HTTP server for
# exposing prometheus metrics (default: :9111).
listen_address: localhost:9111
# long_unconverged_threshold is the duration after which a persistently
# unconverged key will be logged for further investigation (default: 1m).
long_unconverged_threshold: 1m
```

