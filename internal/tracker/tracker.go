package tracker

import (
	"sync"
	"time"
)

// Entry represents a tracked etcd key that is currently unconverged.
type Entry struct {
	// Reason is a human-readable description of the unconverged state.
	Reason string
	// Start is the time at which it was detected that the key is unconverged.
	Start time.Time
}

// Tracker tracks etcd key convergence, and implements
// ledger.ConvergenceTracker.
type Tracker struct {
	// While RWMutex could be used here, there's not really a use case for it to
	// justify the additional overhead.
	mu          sync.Mutex
	unconverged map[string]*Entry
	clock       func() time.Time // overridden in tests
}

// New returns a new Tracker instance.
func New() *Tracker {
	return &Tracker{
		unconverged: make(map[string]*Entry),
		clock:       func() time.Time { return time.Now() },
	}
}

// Converged marks the provided key as converged.
func (t *Tracker) Converged(key string) {
	t.mu.Lock()
	defer t.mu.Unlock()
	delete(t.unconverged, key)
}

// Unconverged marks the provided key as unconverged with the associated reason.
func (t *Tracker) Unconverged(key, reason string) {
	t.mu.Lock()
	defer t.mu.Unlock()
	if e, ok := t.unconverged[key]; ok {
		e.Reason = reason
	} else {
		t.unconverged[key] = &Entry{
			Reason: reason,
			Start:  t.clock(),
		}
	}
}

// Sample returns a sample of all currently inconverged keys, as a map from key
// to Entry.
func (t *Tracker) Sample() map[string]*Entry {
	t.mu.Lock()
	defer t.mu.Unlock()
	s := make(map[string]*Entry)
	for k, e := range t.unconverged {
		s[k] = &Entry{Reason: e.Reason, Start: e.Start}
	}
	return s
}
