package tracker

import (
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func TestTracker(t *testing.T) {
	t0 := time.Now()
	testCases := []struct {
		name string
		ops  func(tr *Tracker, advance func(d time.Duration))
		want map[string]*Entry
	}{
		{
			name: "all converged",
			ops: func(tr *Tracker, advance func(d time.Duration)) {
				tr.Converged("/a")
				tr.Converged("/b")
				tr.Converged("/c")
			},
			want: map[string]*Entry{},
		},
		{
			name: "one unconverged",
			ops: func(tr *Tracker, advance func(d time.Duration)) {
				tr.Converged("/a")
				tr.Converged("/b")
				tr.Converged("/c")
				advance(time.Minute)
				tr.Unconverged("/b", "TIRED")
			},
			want: map[string]*Entry{
				"/b": {Reason: "TIRED", Start: t0.Add(time.Minute)},
			},
		},
		{
			name: "one unconverged and reason changes",
			ops: func(tr *Tracker, advance func(d time.Duration)) {
				tr.Converged("/a")
				tr.Converged("/b")
				tr.Converged("/c")
				advance(time.Minute)
				tr.Unconverged("/b", "TIRED")
				advance(time.Minute) // the Start time should not advance
				tr.Unconverged("/b", "TIRED|GRUMPY")
			},
			want: map[string]*Entry{
				"/b": {Reason: "TIRED|GRUMPY", Start: t0.Add(time.Minute)},
			},
		},
		{
			name: "two unconverged one recovers",
			ops: func(tr *Tracker, advance func(d time.Duration)) {
				tr.Converged("/a")
				tr.Converged("/b")
				tr.Converged("/c")
				advance(time.Minute)
				tr.Unconverged("/b", "TIRED")
				advance(time.Minute)
				tr.Unconverged("/c", "GRUMPY")
				advance(time.Minute)
				tr.Converged("/b")
			},
			want: map[string]*Entry{
				"/c": {Reason: "GRUMPY", Start: t0.Add(2 * time.Minute)},
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			now := t0
			advance := func(d time.Duration) { now = now.Add(d) }
			tr := New()
			tr.clock = func() time.Time { return now }
			tc.ops(tr, advance)
			got := tr.Sample()
			if diff := cmp.Diff(tc.want, got); diff != "" {
				t.Errorf("Stat() returned incorrect values (+got,-want):\n%s", diff)
			}
		})
	}
}
