package checker

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"sync"

	"wikimedia.org/operations/software/etcdchecker/internal/ledger"
	"wikimedia.org/operations/software/etcdchecker/internal/tracker"
	"wikimedia.org/operations/software/etcdchecker/internal/watcher"
)

// Start initiates consistency checks among the clusters associated with the
// provided watchers. Returns a Tracker instance to which unconverged keys are
// reported, and channel that will be closed when watchers have shut down. The
// latter occurs on unrecoverable error, or if the provided context is cancelled.
func Start(ctx context.Context, watchers map[string]watcher.Watcher) (*tracker.Tracker, chan any, error) {
	var clusters []string
	for cluster := range watchers {
		clusters = append(clusters, cluster)
	}

	t := tracker.New()
	l := ledger.New(clusters, t)

	var syncDone sync.WaitGroup
	var mu sync.Mutex
	var errs []error
	for cluster, w := range watchers {
		syncDone.Add(1)
		go func(cluster string, w watcher.Watcher) {
			defer syncDone.Done()
			if err := w.Sync(ctx, func(key string, fingerprint []byte) { l.Put(cluster, key, fingerprint) }); err != nil {
				mu.Lock()
				defer mu.Unlock()
				errs = append(errs, fmt.Errorf("watcher.Sync failed for cluster %q: %v", cluster, err))
			}
		}(cluster, w)
	}
	syncDone.Wait()
	if len(errs) > 0 {
		return nil, nil, fmt.Errorf("sync failed: %v", errors.Join(errs...))
	}
	slog.Info("Initial sync complete")

	watchExit := make(chan any)

	go func() {
		watchCtx, cancel := context.WithCancel(ctx)
		defer cancel()
		var watchDone sync.WaitGroup
		for cluster, w := range watchers {
			watchDone.Add(1)
			go func(cluster string, w watcher.Watcher) {
				defer watchDone.Done()
				put := func(key string, fingerprint []byte) { l.Put(cluster, key, fingerprint) }
				del := func(key string) { l.Delete(cluster, key) }
				if err := w.Watch(watchCtx, put, del); err != nil {
					if errors.Is(err, context.Canceled) {
						return
					}
					slog.Error("Watch failed: cancelling siblings", "cluster", cluster, "error", err)
					cancel() // stop other watches
				}
			}(cluster, w)
		}
		watchDone.Wait()
		close(watchExit)
	}()

	return t, watchExit, nil
}
