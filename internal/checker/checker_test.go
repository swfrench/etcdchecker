package checker_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"wikimedia.org/operations/software/etcdchecker/internal/checker"
	"wikimedia.org/operations/software/etcdchecker/internal/tracker"
	"wikimedia.org/operations/software/etcdchecker/internal/watcher"
)

// sleepingWatcher waits in Watch until cancellation.
type sleepingWatcher struct{}

func (*sleepingWatcher) Sync(context.Context, watcher.PutCallback) error {
	return nil
}

func (*sleepingWatcher) Watch(ctx context.Context, put watcher.PutCallback, del watcher.DeleteCallback) error {
	<-ctx.Done()
	return nil
}

func TestCancellation(t *testing.T) {
	// Cancellation should result in watchers exiting promptly, signalled via
	// the done channel.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, done, err := checker.Start(ctx, map[string]watcher.Watcher{
		"cluster-1": new(sleepingWatcher),
		"cluster-2": new(sleepingWatcher),
	})
	if err != nil {
		t.Fatalf("Start() returned unexpected error: %v", err)
	}
	timeout := 5 * time.Second
	tm := time.NewTimer(timeout)
	defer tm.Stop()
	cancel()
	select {
	case <-done:
	case <-tm.C:
		t.Fatalf("Start() did not signal watcher exit within %v", timeout)
	}
}

// errorWatcher returns prepared errors on calls to Sync or Watch.
type errorWatcher struct {
	syncErr  error
	watchErr error
}

func (w *errorWatcher) Sync(context.Context, watcher.PutCallback) error {
	return w.syncErr
}

func (w *errorWatcher) Watch(ctx context.Context, put watcher.PutCallback, del watcher.DeleteCallback) error {
	return w.watchErr
}

func TestSyncError(t *testing.T) {
	// A Sync error should result in an error being returned by Start.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, _, err := checker.Start(ctx, map[string]watcher.Watcher{
		"cluster-1": &errorWatcher{syncErr: errors.New("nope")},
		"cluster-2": new(sleepingWatcher),
	})
	if err == nil {
		t.Error("Start() returned incorrect error status: got nil, want error")
	}
}

func TestWatchError(t *testing.T) {
	// A Watch error should result in cancellation of all other watchers,
	// signalled by the done callback.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, done, err := checker.Start(ctx, map[string]watcher.Watcher{
		"cluster-1": &errorWatcher{watchErr: errors.New("nope")},
		"cluster-2": new(sleepingWatcher),
	})
	if err != nil {
		t.Fatalf("Start() returned unexpected error: %v", err)
	}
	timeout := 5 * time.Second
	tm := time.NewTimer(timeout)
	defer tm.Stop()
	select {
	case <-done:
	case <-tm.C:
		t.Fatalf("Start() did not signal watcher exit within %v", timeout)
	}
}

type op struct {
	key         string
	fingerprint []byte
}

func opPut(key string, fingerprint []byte) *op {
	return &op{key: key, fingerprint: fingerprint}
}

func opDel(key string) *op {
	return &op{key: key}
}

type fakeWatcher struct {
	syncOps  []*op
	watchOps []*op
}

func (w *fakeWatcher) Sync(ctx context.Context, put watcher.PutCallback) error {
	for _, o := range w.syncOps {
		put(o.key, o.fingerprint)
	}
	return nil
}

func (w *fakeWatcher) Watch(ctx context.Context, put watcher.PutCallback, del watcher.DeleteCallback) error {
	for _, o := range w.watchOps {
		if o.fingerprint == nil {
			del(o.key)
		} else {
			put(o.key, o.fingerprint)
		}
	}
	<-ctx.Done()
	return nil
}

func TestUpdatesTracker(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	tr, done, err := checker.Start(ctx, map[string]watcher.Watcher{
		"cluster-1": &fakeWatcher{
			syncOps: []*op{
				opPut("/x", []byte("a")),
				opPut("/y", []byte("b")),
				opPut("/z", []byte("c")),
			},
			watchOps: []*op{
				opPut("/x", []byte("0")),
				opPut("/y", []byte("1")),
			},
		},
		"cluster-2": &fakeWatcher{
			syncOps: []*op{
				opPut("/x", []byte("a")),
				opPut("/y", []byte("b")),
				opPut("/z", []byte("c")),
			},
			watchOps: []*op{
				opDel("/y"),
			},
		},
	})
	if err != nil {
		t.Fatalf("Start() returned unexpected error: %v", err)
	}
	// Cancel so that fakeWatcher.Watch returns after issuing all ops.
	timeout := 5 * time.Second
	tm := time.NewTimer(timeout)
	cancel()
	defer tm.Stop()
	select {
	case <-done:
	case <-tm.C:
		t.Fatalf("Start() did not signal watcher exit within %v", timeout)
	}
	// Verify the expect outcome is observed by tracker.
	got := tr.Sample()
	for _, e := range got {
		e.Start = time.Time{} // zero out times to facilitate comparison
	}
	want := map[string]*tracker.Entry{
		"/x": {Reason: "CONTENT"},
		"/y": {Reason: "EXISTENCE"},
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Tracker.Sample() returned unexpected result (+got,-want):\n%s", diff)
	}
}
