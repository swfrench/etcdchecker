package config_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"wikimedia.org/operations/software/etcdchecker/internal/config"
)

func TestLoad(t *testing.T) {
	testCases := []struct {
		name     string
		filename string
		want     *config.Config
		wantErr  bool
	}{
		{
			name:     "defaults",
			filename: "testdata/empty.yaml",
			want: &config.Config{
				Prefix:                   config.DefaultPrefix,
				ListenAddress:            config.DefaultListenAddress,
				ReportInterval:           config.DefaultReportInterval,
				LongUnconvergedThreshold: config.DefaultLongUnconvergedThreshold,
			},
		},
		{
			name:     "adds trailing slash",
			filename: "testdata/prefix.yaml",
			want: &config.Config{
				Prefix:                   "/foo/",
				ListenAddress:            config.DefaultListenAddress,
				ReportInterval:           config.DefaultReportInterval,
				LongUnconvergedThreshold: config.DefaultLongUnconvergedThreshold,
			},
		},
		{
			name:     "does not exist",
			filename: "testdata/does_not_exist.yaml",
			wantErr:  true,
		},
		{
			name:     "invalid yaml",
			filename: "testdata/invalid.yaml",
			wantErr:  true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := config.Load(tc.filename)
			if gotErr := err != nil; gotErr != tc.wantErr {
				t.Fatalf("Load(%q) returned incorrect error status: gotErr: %t wantErr: %t (err: %v)", tc.filename, gotErr, tc.wantErr, err)
			}
			if tc.wantErr {
				return
			}
			if diff := cmp.Diff(tc.want, got); diff != "" {
				t.Errorf("Load(%q) returned incorrect Config (+got,-want):\n%s", tc.filename, diff)
			}
		})
	}

}
