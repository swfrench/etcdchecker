package config

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gopkg.in/yaml.v3"
)

const (
	DefaultPrefix                   = "/"
	DefaultListenAddress            = ":9111"
	DefaultReportInterval           = 30 * time.Second
	DefaultLongUnconvergedThreshold = time.Minute
)

// ClusterConfig represents the client configuration for one monitored cluster.
type ClusterConfig struct {
	// API is the etcd API version to use when interacting with these endpoints
	// (v2, v3).
	API string `yaml:"api"`
	// Endpoints is a list of endpoint URLs.
	Endpoints []string `yaml:"endpoints"`
	// Username is the username to use when authenticating to the cluster
	// (optional).
	Username string `yaml:"username"`
	// Password is the password to use when authenticating to the cluster
	// (optional).
	Password string `yaml:"password"`
	// Optional root CA certificates to use when verifying certificates
	// presented by etcd nodes in this cluster.
	CACert string `yaml:"ca_cert"`

	// TODO: Consider support for DNS SRV endpoint discovery (net.LookupSRV)
}

// Config represents the configuration of the consistency checker.
type Config struct {
	// Clusters is a map from convenient name of a monitored cluster to client
	// configuration used to interact with it.
	Clusters map[string]*ClusterConfig `yaml:"clusters"`
	// Prefix is the monitored keyspace prefix.
	Prefix string `yaml:"prefix"`
	// IgnoredPattern is a regexp (RE2) used to ignore certain parts of the
	// keyspace (optional).
	IgnoredPattern string `yaml:"ignored_pattern"`
	// ListenAddress is the address on which the checker spawns an HTTP server
	// for exposing prometheus metrics.
	ListenAddress string `yaml:"listen_address"`
	// ReportInterval is the duration between updates to the consistency
	// prometheus metrics (and logging for long-unconverged keys).
	ReportInterval time.Duration `yaml:"report_interval"`
	// LongUnconvergedThreshold is the duration after which a persistently
	// unconverged key will be logged for further investigation.
	LongUnconvergedThreshold time.Duration `yaml:"long_unconverged_threshold"`

	// TODO: TLS configuration for the HTTP server.
}

// Load loads the consistency checker config from the provided filename.
func Load(filename string) (*Config, error) {
	bs, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file: %v", err)
	}
	c := &Config{
		Prefix:                   DefaultPrefix,
		ListenAddress:            DefaultListenAddress,
		ReportInterval:           DefaultReportInterval,
		LongUnconvergedThreshold: DefaultLongUnconvergedThreshold,
	}
	if err := yaml.Unmarshal(bs, c); err != nil {
		return nil, fmt.Errorf("failed to unmarshal config file: %v", err)
	}
	// TODO: We could potentially validate a lot more (e.g., need at least two
	// clusters, each needs endpoints and API, etc.).
	if !strings.HasSuffix(c.Prefix, "/") {
		c.Prefix = fmt.Sprintf("%s/", c.Prefix)
	}
	return c, nil
}
