package watcher

import (
	"context"
	"crypto/tls"
	"fmt"
	"regexp"
	"time"

	"go.etcd.io/etcd/api/v3/mvccpb"
	clientv3 "go.etcd.io/etcd/client/v3"
)

type V3 struct {
	api     *clientv3.Client
	prefix  string
	ignored *regexp.Regexp
	syncRev int64
}

func NewV3(endpoints []string, prefix, username, password string, ignored *regexp.Regexp, tlsConfig *tls.Config) (Watcher, error) {
	api, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: 5 * time.Second,
		Username:    username,
		Password:    password,
		TLS:         tlsConfig,
	})
	if err != nil {
		return nil, err
	}
	return &V3{
		api:     api,
		prefix:  prefix,
		ignored: ignored,
	}, nil
}

func (w *V3) Sync(ctx context.Context, put PutCallback) error {
	// Note: unlike the v2 API, v3 gives us the opportunity to limit response
	// size (at least in terms of number of kvs). If we run into issues with
	// large responses (and particularly if we can deprecate the v2 watcher),
	// consider using this mechanism for an incremental sync.
	kvc := clientv3.NewKV(w.api)
	resp, err := kvc.Get(ctx, w.prefix, clientv3.WithPrefix())
	if err != nil {
		return err
	}
	for _, kv := range resp.Kvs {
		if w.ignored != nil && w.ignored.Match(kv.Key) {
			continue
		}
		put(string(kv.Key), valueHash(kv.Value))
	}
	w.syncRev = resp.Header.Revision
	return nil
}

func (w *V3) Watch(ctx context.Context, put PutCallback, del DeleteCallback) error {
	if w.syncRev == 0 {
		return fmt.Errorf("cannot invoke Watch before Sync")
	}
	wc := clientv3.NewWatcher(w.api)
	for resp := range wc.Watch(ctx, w.prefix, clientv3.WithPrefix(), clientv3.WithRev(w.syncRev+1), clientv3.WithProgressNotify()) {
		if err := resp.Err(); err != nil {
			return err
		}
		if resp.IsProgressNotify() {
			w.syncRev = resp.Header.Revision
			continue
		}
		for _, event := range resp.Events {
			w.syncRev = max(w.syncRev, event.Kv.ModRevision)
			if w.ignored != nil && w.ignored.Match(event.Kv.Key) {
				continue
			}
			switch event.Type {
			case mvccpb.PUT:
				put(string(event.Kv.Key), valueHash(event.Kv.Value))
			case mvccpb.DELETE:
				del(string(event.Kv.Key))
			}
		}
	}
	return nil
}
