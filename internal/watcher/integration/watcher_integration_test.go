// Package watcher_test provides simple integration tests for v2 and v3
// Watchers. This is preferable to, e.g., implementing client test doubles.
//
// The address of a running v2-capable etcd node must be provided, or otherwise
// the tests are skipped (TEST_ETCD_ENDPOINT).
//
// While it would be possible to use an embedded etcd server, that adds a large
// number of otherwise-unused dependencies.
package watcher_test

import (
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"regexp"
	"sync"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	clientv2 "go.etcd.io/etcd/client/v2"
	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdchecker/internal/watcher"
)

const (
	endpointEnvVar = "TEST_ETCD_ENDPOINT"
	cleanupTimeout = 5 * time.Second
)

func endpoints() []string {
	return []string{os.Getenv(endpointEnvVar)}
}

// mustCreateV3ClientWithCleanup creates and returns a v3 Client. The keyspace
// will be cleared and the client closed on test completion.
func mustCreateV3ClientWithCleanup(t *testing.T) *clientv3.Client {
	api, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints(),
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		t.Fatalf("Failed to initialize v3 client: %v", err)
	}
	t.Cleanup(func() {
		defer api.Close()
		ctx, cancel := context.WithTimeout(context.Background(), cleanupTimeout)
		defer cancel()
		dr, err := api.KV.Delete(ctx, "/", clientv3.WithPrefix())
		if err != nil {
			t.Fatalf("Failed to clean up etcd state: %v", err)
		}
		t.Logf("Deleted %d v3 keys during cleanup", dr.Deleted)
	})
	return api
}

// mustCreateV2ClientWithCleanup creates and returns a v2 KeysAPI client. The
// keyspace will be cleared on test completion.
func mustCreateV2ClientWithCleanup(t *testing.T) clientv2.KeysAPI {
	c, err := clientv2.New(clientv2.Config{
		Endpoints: endpoints(),
		Transport: clientv2.DefaultTransport,
	})
	if err != nil {
		t.Fatalf("Failed to initialize v2 client: %v", err)
	}
	api := clientv2.NewKeysAPI(c)
	t.Cleanup(func() {
		ctx, cancel := context.WithTimeout(context.Background(), cleanupTimeout)
		defer cancel()
		gr, err := api.Get(ctx, "/", nil)
		if err != nil {
			t.Fatalf("Failed to query etcd state in cleanup: %v", err)
		}
		for _, n := range gr.Node.Nodes {
			_, err := api.Delete(ctx, n.Key, &clientv2.DeleteOptions{Recursive: true})
			if err != nil {
				t.Fatalf("Failed to clean up etcd state: %v", err)
			}
		}
		t.Logf("Recursively deleted %d top-level v2 keys during cleanup", len(gr.Node.Nodes))
	})
	return api
}

type op struct {
	key   string
	value string
}

func opPut(key, value string) *op {
	return &op{key: key, value: value}
}

func opDel(key string) *op {
	return &op{key: key}
}

func mustApplyOpsV3(t *testing.T, c *clientv3.Client, ops []*op) {
	for _, o := range ops {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		if o.value == "" {
			if _, err := c.KV.Delete(ctx, o.key); err != nil {
				t.Fatalf("Delete op failed: %v", err)
			}
		} else {
			if _, err := c.KV.Put(ctx, o.key, o.value); err != nil {
				t.Fatalf("Put op failed: %v", err)
			}
		}
	}
}

func mustApplyOpsV2(t *testing.T, c clientv2.KeysAPI, ops []*op) {
	for _, o := range ops {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		if o.value == "" {
			if _, err := c.Delete(ctx, o.key, nil); err != nil {
				t.Fatalf("Delete op failed: %v", err)
			}
		} else {
			if _, err := c.Set(ctx, o.key, o.value, nil); err != nil {
				t.Fatalf("Put op failed: %v", err)
			}
		}
	}
}

type opLog struct {
	wg  *sync.WaitGroup
	ops []string
}

func (o *opLog) put(key string, fingerprint []byte) {
	o.ops = append(o.ops, fmt.Sprintf("PUT(%s, %s)", key, hex.EncodeToString(fingerprint)))
	if o.wg != nil {
		o.wg.Done()
	}
}

func (o *opLog) del(key string) {
	o.ops = append(o.ops, fmt.Sprintf("DEL(%s)", key))
	if o.wg != nil {
		o.wg.Done()
	}
}

func TestSync(t *testing.T) {
	ops := []*op{
		opPut("/foo", "1"),
		opPut("/bar", "2"),
		opPut("/baz", "3"),
	}
	ignored := regexp.MustCompile("^/bar$")
	want := []string{
		"PUT(/foo, 6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b)",
		"PUT(/baz, 4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce)",
	}
	testCases := []struct {
		name    string
		actions func(t *testing.T)
		watcher watcher.WatcherFactory
	}{
		{
			name: "v2",
			actions: func(t *testing.T) {
				c := mustCreateV2ClientWithCleanup(t)
				mustApplyOpsV2(t, c, ops)
			},
			watcher: watcher.NewV2,
		},
		{
			name: "v3",
			actions: func(t *testing.T) {
				c := mustCreateV3ClientWithCleanup(t)
				mustApplyOpsV3(t, c, ops)
			},
			watcher: watcher.NewV3,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.actions(t)
			w, err := tc.watcher(endpoints(), "/", "", "", ignored, nil)
			if err != nil {
				t.Fatalf("Failed to initialize watcher: %v", err)
			}
			l := new(opLog)
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			if err := w.Sync(ctx, l.put); err != nil {
				t.Fatalf("Failed to Sync watcher: %v", err)
			}
			// Sync can occur in any order.
			opt := cmpopts.SortSlices(func(a, b string) bool { return a < b })
			if diff := cmp.Diff(want, l.ops, opt); diff != "" {
				t.Errorf("Sync() produced an incorrect op sequence (+got,-want):\n%s", diff)
			}
		})
	}
}

type opGroups struct {
	sync  func(t *testing.T)
	watch func(t *testing.T)
}

func TestWatch(t *testing.T) {
	ops := []*op{
		opPut("/foo", "1"),
		opPut("/bar", "2"),
		opPut("/baz", "3"),
		opDel("/bar"),
		opPut("/foo", "4"),
		opPut("/bar", "5"),
		opDel("/foo"),
	}
	ignored := regexp.MustCompile("^/bar$")
	want := []string{
		"PUT(/foo, 6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b)",
		"PUT(/baz, 4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce)",
		"PUT(/foo, 4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a)",
		"DEL(/foo)",
	}
	testCases := []struct {
		name    string
		groups  func(t *testing.T) opGroups
		watcher watcher.WatcherFactory
		want    []string
		opts    []cmp.Option
	}{
		{
			name: "v3",
			groups: func(t *testing.T) opGroups {
				c := mustCreateV3ClientWithCleanup(t)
				return opGroups{
					sync: func(t *testing.T) {},
					watch: func(t *testing.T) {
						mustApplyOpsV3(t, c, ops)
					},
				}
			},
			watcher: watcher.NewV3,
			want:    want,
		},
		{
			name: "v2",
			groups: func(t *testing.T) opGroups {
				c := mustCreateV2ClientWithCleanup(t)
				return opGroups{
					sync: func(t *testing.T) {},
					watch: func(t *testing.T) {
						mustApplyOpsV2(t, c, ops)
					},
				}
			},
			watcher: watcher.NewV2,
			want:    want,
		},
		{
			name: "v2 recursive delete",
			groups: func(t *testing.T) opGroups {
				c := mustCreateV2ClientWithCleanup(t)
				return opGroups{
					sync: func(t *testing.T) {
						mustApplyOpsV2(t, c, []*op{
							opPut("/foo/a", "1"),
							opPut("/foo/b", "2"),
							opPut("/foo/c", "3"),
							opDel("/foo/b"),
							opPut("/foooo", "4"),
						})
					},
					watch: func(t *testing.T) {
						ctx, cancel := context.WithTimeout(context.Background(), time.Second)
						defer cancel()
						if _, err := c.Delete(ctx, "/foo", &clientv2.DeleteOptions{Recursive: true}); err != nil {
							t.Fatalf("Recursive delete failed: %v", err)
						}
					},
				}
			},
			watcher: watcher.NewV2,
			want: []string{
				"DEL(/foo/a)",
				"DEL(/foo/c)",
			},
			// Deletion notification can happen in any order.
			opts: []cmp.Option{
				cmpopts.SortSlices(func(a, b string) bool { return a < b }),
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			w, err := tc.watcher(endpoints(), "/", "", "", ignored, nil)
			if err != nil {
				t.Fatalf("Failed to initialize watcher: %v", err)
			}

			g := tc.groups(t)
			g.sync(t)

			syncCtx, syncCancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer syncCancel()
			if err := w.Sync(syncCtx, func(key string, fingerprint []byte) {}); err != nil {
				t.Fatalf("Failed to Sync watcher: %v", err)
			}

			var wg sync.WaitGroup
			wg.Add(len(tc.want))
			l := &opLog{wg: &wg}

			var watchErr error
			done := make(chan any)
			watchCtx, watchCancel := context.WithCancel(context.Background())
			defer watchCancel()
			go func() {
				defer close(done)
				err := w.Watch(watchCtx, l.put, l.del)
				if err != nil && !errors.Is(err, context.Canceled) {
					watchErr = err
				}
			}()

			// cancel the watch when all expected ops have been observed
			go func() {
				wg.Wait()
				watchCancel()
			}()

			g.watch(t)

			// wait for the watch to return
			tm := time.NewTimer(10 * time.Second)
			defer tm.Stop()
			select {
			case <-done:
			case <-tm.C:
				t.Fatal("Watch did not terminate within the timeout")
			}
			if watchErr != nil {
				t.Fatalf("Watch returned unexpected error: %v", watchErr)
			}

			if diff := cmp.Diff(tc.want, l.ops, tc.opts...); diff != "" {
				t.Errorf("Watch() produced an incorrect op sequence (+got,-want):\n%s", diff)
			}
		})
	}
}

func TestWatchNoSyncError(t *testing.T) {
	testCases := []struct {
		name    string
		watcher watcher.WatcherFactory
	}{
		{
			name:    "v3",
			watcher: watcher.NewV3,
		},
		{
			name:    "v2",
			watcher: watcher.NewV2,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			w, err := tc.watcher(endpoints(), "/", "", "", nil, nil)
			if err != nil {
				t.Fatalf("Failed to initialize watcher: %v", err)
			}

			l := new(opLog)

			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			err = w.Watch(ctx, l.put, l.del)
			if err == nil || errors.Is(err, context.DeadlineExceeded) {
				t.Errorf("Watch() returned incorrect error status: got %v, want (non-deadline) error", err)
			}
		})
	}
}

func TestMain(m *testing.M) {
	endpoint := os.Getenv(endpointEnvVar)
	if endpoint == "" {
		fmt.Printf("Skipping Watcher tests: %s unset", endpointEnvVar)
		os.Exit(0)
	}
	os.Exit(m.Run())
}
