package watcher

import (
	"context"
	"crypto/sha256"
	"crypto/tls"
	"regexp"
)

// PutCallback is the signature of the callback function used to notify on key
// content change.
type PutCallback func(key string, fingerprint []byte)

// DeleteCallback is the signature of the callback function used to notify on
// key deletion.
type DeleteCallback func(key string)

// WatcherFactory is the signature of the factory function provided by each
// Watcher implementation.
type WatcherFactory func(endpoints []string, prefix, username, password string, ignored *regexp.Regexp, tlsConfig *tls.Config) (Watcher, error)

// Watcher is an abstract watcher over a configured prefix.
type Watcher interface {
	// Sync will read the entire keyspace below the configured prefix, invoking
	// the put callback for each key.
	Sync(context.Context, PutCallback) error
	// Watch will watch the entire keyspace below the configured prefix,
	// starting at the most recent etcd index (initially, that at which Sync
	// completed), invoking the put and delete callbacks for key updates.
	// If Watch returns an error, it should be assumed that the Watcher instance
	// can no longer be used, and a new one should be constructed.
	Watch(context.Context, PutCallback, DeleteCallback) error
}

// valueHash is the fingerprint scheme used by all Watcher implementations.
func valueHash(value []byte) []byte {
	h := sha256.New()
	h.Write([]byte(value))
	return h.Sum(nil)
}
