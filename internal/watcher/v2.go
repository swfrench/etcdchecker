package watcher

import (
	"context"
	"crypto/tls"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"go.etcd.io/etcd/client/v2"
)

type V2 struct {
	api     client.KeysAPI
	prefix  string
	ignored *regexp.Regexp
	// We need to track observed keys in order to handle recursive deletes.
	// A map is fast in the common case (single-key insert / delete), at the
	// expense of storage, but makes recursive deletes a linear scan. On the
	// basis that such operations are rare, defaulting to a map makes sense.
	// If that changes, consider a trie (perhaps github.com/dghubble/trie).
	keys      map[string]any
	syncIndex uint64
}

func NewV2(endpoints []string, prefix, username, password string, ignored *regexp.Regexp, tlsConfig *tls.Config) (Watcher, error) {
	transport := client.DefaultTransport
	if tlsConfig != nil {
		// With the exception of TLSClientConfig, this is identical to the
		// definition of client.DefaultTransport.
		transport = &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			Dial: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 10 * time.Second,
			TLSClientConfig:     tlsConfig,
		}
	}
	c, err := client.New(client.Config{
		Endpoints: endpoints,
		Transport: transport,
		Username:  username,
		Password:  password,
	})
	if err != nil {
		return nil, err
	}
	return &V2{
		api:     client.NewKeysAPI(c),
		prefix:  prefix,
		ignored: ignored,
		keys:    make(map[string]any),
	}, nil
}

func forEachLeaf(n *client.Node, fn func(*client.Node)) {
	if n.Dir {
		for i := range n.Nodes {
			forEachLeaf(n.Nodes[i], fn)
		}
	} else {
		fn(n)
	}
}

func (w *V2) Sync(ctx context.Context, put PutCallback) error {
	resp, err := w.api.Get(ctx, w.prefix, &client.GetOptions{
		Recursive: true,
		Quorum:    true,
	})
	if err != nil {
		return err
	}
	w.syncIndex = resp.Index
	forEachLeaf(resp.Node, func(n *client.Node) {
		if w.ignored != nil && w.ignored.MatchString(n.Key) {
			return
		}
		put(n.Key, valueHash([]byte(n.Value)))
		w.keys[n.Key] = nil
	})
	return nil
}

func (w *V2) Watch(ctx context.Context, put PutCallback, del DeleteCallback) error {
	if w.syncIndex == 0 {
		return fmt.Errorf("cannot invoke Watch before Sync")
	}
	watch := func() error {
		watcher := w.api.Watcher(w.prefix, &client.WatcherOptions{
			Recursive:  true,
			AfterIndex: w.syncIndex,
		})
		for {
			resp, err := watcher.Next(ctx)
			if err != nil {
				slog.Error("Watch failed", "error", err)
				etcdErr, ok := err.(client.Error)
				if ok && etcdErr.Code == client.ErrorCodeEventIndexCleared {
					// Recovery from a cleared wait index isn't straightforward.
					// Instead, terminate to force a sync.
					return backoff.Permanent(err)
				}
				return err
			}
			switch resp.Action {
			case "set", "update", "create", "compareAndSwap":
				if resp.Node.Dir {
					// We do not care, at least for now.
				} else {
					if w.ignored != nil && w.ignored.MatchString(resp.Node.Key) {
						continue
					}
					put(resp.Node.Key, valueHash([]byte(resp.Node.Value)))
					w.keys[resp.Node.Key] = nil
				}
			case "delete", "expire", "compareAndDelete":
				if resp.Node.Dir {
					// Note: No need to check ignored here, as we're iterating over
					// non-ignored keys.
					dk := fmt.Sprintf("%s/", resp.Node.Key)
					for k := range w.keys {
						if strings.HasPrefix(k, dk) {
							del(k)
							delete(w.keys, k)
						}
					}
				} else {
					if w.ignored != nil && w.ignored.MatchString(resp.Node.Key) {
						continue
					}
					del(resp.Node.Key)
					delete(w.keys, resp.Node.Key)
				}
			default:
				return fmt.Errorf("watch returned unsupported action: %s", resp.Action)
			}
			w.syncIndex = resp.Node.ModifiedIndex
		}
	}
	// Unlike the v3 client, where gRPC will manage reconnects should the
	// service become temporarily unavailable, this is not the case in v2.
	// Instead, manage retries directly (for up to 1m).
	opts := []backoff.ExponentialBackOffOpts{
		backoff.WithInitialInterval(5 * time.Second),
		backoff.WithMultiplier(1.5),
		backoff.WithRandomizationFactor(0.2),
		backoff.WithMaxElapsedTime(time.Minute),
	}
	return backoff.Retry(watch, backoff.NewExponentialBackOff(opts...))
}
