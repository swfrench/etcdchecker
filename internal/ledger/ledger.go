package ledger

import (
	"bytes"
	"log/slog"
	"strings"
	"sync"
)

type entry struct {
	fingerprint map[string][]byte
}

type Difference uint

const (
	DifferenceNone      Difference = 0
	DifferenceContent   Difference = 1
	DifferenceExistence Difference = 2
)

func (d Difference) String() string {
	if d == DifferenceNone {
		return "NONE"
	}
	var diffs []string
	if d&DifferenceContent == DifferenceContent {
		diffs = append(diffs, "CONTENT")
	}
	if d&DifferenceExistence == DifferenceExistence {
		diffs = append(diffs, "EXISTENCE")
	}
	return strings.Join(diffs, "|")
}

func (e *entry) validate(clusters []string) Difference {
	if len(e.fingerprint) == 0 {
		// Consistent: key is not present in any cluster
		return DifferenceNone
	}
	ret := DifferenceNone
	var firstFingerprint []byte
	for _, cluster := range clusters {
		fingerprint, ok := e.fingerprint[cluster]
		if ok {
			if firstFingerprint == nil {
				firstFingerprint = fingerprint
			} else if !bytes.Equal(fingerprint, firstFingerprint) {
				// Inconsistent: key has different content across clusters.
				ret |= DifferenceContent
			}
		} else {
			// Inconsistent: key is not present in a subset of clusters.
			ret |= DifferenceExistence
		}
	}
	return ret
}

type ConvergenceTracker interface {
	Converged(key string)
	Unconverged(key, reason string)
}

type Ledger struct {
	clusters []string
	tracker  ConvergenceTracker
	mu       sync.Mutex
	content  map[string]*entry
}

func New(clusters []string, tracker ConvergenceTracker) *Ledger {
	return &Ledger{
		clusters: clusters,
		tracker:  tracker,
		content:  make(map[string]*entry),
	}
}

func (l *Ledger) validate(key string, e *entry) {
	diff := e.validate(l.clusters)
	if diff == DifferenceNone {
		l.tracker.Converged(key)
	} else {
		reason := diff.String()
		slog.Debug("Key is unconverged across clusters", "key", key, "reason", reason)
		l.tracker.Unconverged(key, reason)
	}
}

func (l *Ledger) Put(cluster, key string, fingerprint []byte) {
	l.mu.Lock()
	defer l.mu.Unlock()
	e, ok := l.content[key]
	if !ok {
		e = &entry{fingerprint: make(map[string][]byte)}
		l.content[key] = e
	}
	e.fingerprint[cluster] = fingerprint
	l.validate(key, e)
}

func (l *Ledger) Delete(cluster, key string) {
	l.mu.Lock()
	defer l.mu.Unlock()
	e := l.content[key]
	delete(e.fingerprint, cluster)
	l.validate(key, e)
	if len(e.fingerprint) == 0 {
		delete(l.content, key)
	}
}
