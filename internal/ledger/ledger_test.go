package ledger_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"wikimedia.org/operations/software/etcdchecker/internal/ledger"
)

type op func(*ledger.Ledger)

func opPut(cluster, key string, fingerprint []byte) op {
	return func(l *ledger.Ledger) { l.Put(cluster, key, fingerprint) }
}

func opDel(cluster, key string) op {
	return func(l *ledger.Ledger) { l.Delete(cluster, key) }
}

type update struct {
	Converged bool
	Key       string
	Reason    string
}

type fakeTracker struct {
	updates []update
}

func (t *fakeTracker) Converged(key string) {
	t.updates = append(t.updates, update{Converged: true, Key: key})
}

func (t *fakeTracker) Unconverged(key, reason string) {
	t.updates = append(t.updates, update{Converged: false, Key: key, Reason: reason})
}

func TestLedger(t *testing.T) {
	clusters := []string{
		"cluster-a",
		"cluster-b",
		"cluster-c",
	}
	testCases := []struct {
		name string
		ops  []op
		want []update
	}{
		{
			name: "converged no keys",
		},
		{
			name: "converged content match",
			ops: []op{
				opPut("cluster-a", "/key", []byte("hash")),
				opPut("cluster-b", "/key", []byte("hash")),
				opPut("cluster-c", "/key", []byte("hash")),
			},
			want: []update{
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: true, Key: "/key"},
			},
		},
		{
			name: "converged content match update",
			ops: []op{
				opPut("cluster-a", "/key", []byte("hash")),
				opPut("cluster-b", "/key", []byte("hash")),
				opPut("cluster-c", "/key", []byte("hash")),
				opPut("cluster-a", "/key", []byte("new-hash")),
				opPut("cluster-b", "/key", []byte("new-hash")),
				opPut("cluster-c", "/key", []byte("new-hash")),
			},
			want: []update{
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: true, Key: "/key"},
				{Converged: false, Key: "/key", Reason: "CONTENT"},
				{Converged: false, Key: "/key", Reason: "CONTENT"},
				{Converged: true, Key: "/key"},
			},
		},
		{
			name: "converged existence match delete",
			ops: []op{
				opPut("cluster-a", "/key", []byte("hash")),
				opPut("cluster-b", "/key", []byte("hash")),
				opPut("cluster-c", "/key", []byte("hash")),
				opDel("cluster-a", "/key"),
				opDel("cluster-b", "/key"),
				opDel("cluster-c", "/key"),
			},
			want: []update{
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: true, Key: "/key"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: true, Key: "/key"},
			},
		},
		{
			name: "unconverged content mismatch",
			ops: []op{
				opPut("cluster-a", "/key", []byte("hash")),
				opPut("cluster-b", "/key", []byte("hash")),
				opPut("cluster-c", "/key", []byte("hash")),
				opPut("cluster-a", "/key", []byte("new-hash")),
			},
			want: []update{
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: true, Key: "/key"},
				{Converged: false, Key: "/key", Reason: "CONTENT"},
			},
		},
		{
			name: "unconverged existence mismatch",
			ops: []op{
				opPut("cluster-a", "/key", []byte("hash")),
				opPut("cluster-b", "/key", []byte("hash")),
				opPut("cluster-c", "/key", []byte("hash")),
				opDel("cluster-a", "/key"),
			},
			want: []update{
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: true, Key: "/key"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
			},
		},
		{
			name: "unconverged content and existence mismatch",
			ops: []op{
				opPut("cluster-a", "/key", []byte("hash")),
				opPut("cluster-b", "/key", []byte("hash")),
				opPut("cluster-c", "/key", []byte("hash")),
				opDel("cluster-a", "/key"),
				opPut("cluster-b", "/key", []byte("new-hash")),
				opPut("cluster-c", "/key", []byte("different-new-hash")),
			},
			want: []update{
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: true, Key: "/key"},
				{Converged: false, Key: "/key", Reason: "EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "CONTENT|EXISTENCE"},
				{Converged: false, Key: "/key", Reason: "CONTENT|EXISTENCE"},
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tr := &fakeTracker{}
			l := ledger.New(clusters, tr)
			for _, op := range tc.ops {
				op(l)
			}
			if diff := cmp.Diff(tc.want, tr.updates); diff != "" {
				t.Errorf("Ledger returned incorrect convergence reports (+got,-want):\n%s", diff)
			}
		})
	}
}
