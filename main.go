// etcdchecker is a simple cross-cluster etcd consistency checker.
package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"regexp"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"wikimedia.org/operations/software/etcdchecker/internal/checker"
	"wikimedia.org/operations/software/etcdchecker/internal/config"
	"wikimedia.org/operations/software/etcdchecker/internal/watcher"
)

var (
	configFile = flag.String("config", "checker.yaml", "Path to YAML format config file.")
	logLevel   = flag.String("log_level", "INFO", "The slog-compatible log-level to use.")
)

func mustCompileIgnored(pattern string) *regexp.Regexp {
	if pattern == "" {
		return nil
	}
	return regexp.MustCompile(pattern)
}

func loadPEM(filename string) (*x509.CertPool, error) {
	bs, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to load CA certs file: %v", err)
	}
	pool := x509.NewCertPool()
	if !pool.AppendCertsFromPEM(bs) {
		return nil, fmt.Errorf("failed to load PEM content from %q", filename)
	}
	return pool, nil
}

func mustCreateWatchers(conf *config.Config) map[string]watcher.Watcher {
	ignored := mustCompileIgnored(conf.IgnoredPattern)
	watchers := make(map[string]watcher.Watcher)
	for cluster, clusterConf := range conf.Clusters {
		slog.Info("Initializing watcher", "cluster", cluster, "api", clusterConf.API)
		var f watcher.WatcherFactory
		switch clusterConf.API {
		case "v2":
			f = watcher.NewV2
		case "v3":
			f = watcher.NewV3
		default:
			slog.Error("Config contained unsupported API", "cluster", cluster, "api", clusterConf.API)
			os.Exit(1)
		}
		var tc *tls.Config
		if clusterConf.CACert != "" {
			certs, err := loadPEM(clusterConf.CACert)
			if err != nil {
				slog.Error("Failed to load CACert file", "cluster", cluster, "file", clusterConf.CACert, "error", err)
				os.Exit(1)
			}
			tc = &tls.Config{RootCAs: certs}
		}
		w, err := f(clusterConf.Endpoints, conf.Prefix, clusterConf.Username, clusterConf.Password, ignored, tc)
		if err != nil {
			slog.Error("Failed to initialize watcher", "cluster", cluster, "api", clusterConf.API, "endpoints", clusterConf.Endpoints, "error", err)
			os.Exit(1)
		}
		watchers[cluster] = w
	}
	return watchers
}

func main() {
	flag.Parse()

	level := new(slog.Level)
	if err := level.UnmarshalText([]byte(*logLevel)); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to unmarshal slog LogLevel: %v\n", err)
		os.Exit(1)
	}
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{Level: level})))

	conf, err := config.Load(*configFile)
	if err != nil {
		slog.Error("Failed to load config", "error", err)
		os.Exit(1)
	}

	ctx := context.Background()
	tr, watcherExit, err := checker.Start(ctx, mustCreateWatchers(conf))
	if err != nil {
		slog.Error("Failed to initialize checker", "error", err)
		os.Exit(1)
	}

	reg := prometheus.NewRegistry()
	numUnconvergedMetric := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "etcdchecker_num_unconverged_keys",
		Help: "The number of currently unconverged keys.",
	})
	reg.MustRegister(numUnconvergedMetric)
	maxConvergenceDelayMetric := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "etcdchecker_max_convergence_delay_seconds",
		Help: "The maximum time any currently unconverged key has been in that state.",
	})
	reg.MustRegister(maxConvergenceDelayMetric)

	// Reporter goroutine collects and reports (e.g., to prometheus) periodic
	// stats on tracked unconverged keys.
	go func() {
		tk := time.NewTicker(conf.ReportInterval)
		var numUnconverged int
		for range tk.C {
			s := tr.Sample()
			numUnconvergedMetric.Set(float64(len(s)))
			if len(s) != numUnconverged {
				numUnconverged = len(s)
				if numUnconverged == 0 {
					slog.Info("All keys are converged")
				} else {
					slog.Warn("One or more keys are unconverged", "num_unconverged", numUnconverged)
				}
			}
			maxDelay := time.Duration(0)
			for k, e := range s {
				d := time.Since(e.Start)
				if d >= conf.LongUnconvergedThreshold {
					slog.Error("Long-unconverged", "key", k, "delay", d, "reason", e.Reason)
				}
				maxDelay = max(maxDelay, d)
			}
			maxConvergenceDelayMetric.Set(maxDelay.Seconds())
		}
	}()

	// This goroutine exists purely to detect the case where the watchers have
	// unexpectedly exited (i.e., in response to an irrecoverable error).
	go func() {
		<-watcherExit
		slog.Error("Watchers have exited unexpectedly")
		os.Exit(1)
	}()

	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{Registry: reg}))
	if err := http.ListenAndServe(conf.ListenAddress, mux); err != nil {
		slog.Error("ListenAndServe failed", "error", err)
		os.Exit(1)
	}
}
